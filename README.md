Module audio pure-data pour le Téléscope Harmonique
==

## Installation

```
sudo apt-get install pure-data
```

## Configuration jack

audiorate: 48000

### Pure data autorun au boot avec systemd

créer le fichier =>

```
sudo nano /etc/systemd/system/puredata.service
```

y écrire =>

```
UMask=007
ExecStart=/usr/bin/pd -nogui -jack -r 48000 -noadc -outchannels 2 -open /home/.../.../00_menu.pd
Restart=always
StandardOutput=/var/log/puredata.log
StandardError=/var/log/puredata.log
# Configures the time to wait before service is stopped forcefully.
TimeoutStopSec=300
[Install]
WantedBy=multi-user.target
```

enregistrer, fermer =>

valider le démarage de puredata au boot du pi=>
```
sudo systemctl enable puredata.service
```


#### Commande utiles:

Lancer puredata=>
```
sudo systemctl start puredata
```


Relancer puredata=>
```
sudo systemctl restart puredata
```


Arrêter puredata=>
```
sudo systemctl stop puredata
```


Observer ce que puredata raconte=>

```
journalctl -f --unit puredata

```

### Jack autorun au boot

Pas testé mais trouvé ça -> https://bbs.archlinux.org/viewtopic.php?id=165545

créer le fichier =>

```
sudo nano /etc/systemd/system/jack.service

```

y écrire =>

```
[Unit]
Description=JACK
After=sound.target

[Service]
LimitRTPRIO=infinity
LimitMEMLOCK=infinity
User=james
ExecStart=/usr/bin/jackd -R -P89 -dalsa -hw:1 -r48000 -p128 -n3

[Install]
WantedBy=multi-user.target
```

enregistrer, fermer =>

valider le démarage de puredata au boot du pi=>
```
sudo systemctl enable jack.service
```

## Execution

```
pure-data -jack 00_menu.pd
```


