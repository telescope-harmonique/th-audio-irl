#!/bin/bash

sleep 3
pacmd set-default-source "jack_in"
pacmd set-default-sink "jack_out"

sleep 3
# puredata -jack /home/thadmin/th/th-audio-irl/00_menu.pd
puredata -jack -noadc -outchannels 4 -open /home/thadmin/th/th-audio-irl/00_menu.pd
